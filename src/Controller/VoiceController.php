<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Annotation\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\InputStrings;
use App\Entity\NexmoCredential;
use App\Entity\Survey;
use App\Entity\Feedback;
use App\Entity\Manufacturer;
use App\Entity\Question;

$GLOBALS = array('ncco'=>null);
class VoiceController extends AbstractController
{

    /**
     * @Route("/voice", name="voice")
     */
    public function index()
    {
        return $this->render('voice/index.html.twig', [
            'controller_name' => 'VoiceController',
        ]);
    }
/**
 * @Route("/v/add/", name="addQst")
 */
public function addQuestion(Request $request)
{
	$qst = new Question();
	$man = new Manufacturer();

//	$q1 = "Hello and welcome to fair makers employee survey, please take a few minutes to answer the following questions";
	$qst->setQuestion1("Hello and welcome to fair makers employee survey, please take a few minutes to answer the following questions");
	$qst->setQuestion2("Does your organization respect the prohibition of children working on the site?, Press 0 followed by hash for yes, 1 followed by hash for no.");	
	$qst->setQuestion3("Have you been abused in the last 30 days?, Press 0 followed by hash for yes, 1 followed by hash for no.");	
	$qst->setQuestion4("For the next questions choose a value between 1 and 5, with 1 being worst condition and 5 being the best condition.");	
	$qst->setQuestion5("Do you exceed the legal work hours?, Choose a value from 1 to 5 followed by the hash key.");	
	$qst->setQuestion6("Do you have the right to join a union or workers representative association?, Choose a value from 1 to 5 followed by the hash key.");	
	$qst->setQuestion7("Does your salary correspond at least to the minimum wage of your home country? , Choose a value from 1 to 5 followed by the hash key.");	
	$qst->setQuestion8("Have you identified a lack of hygiene on your site?, Choose a value from 1 to 5 followed by the hash key.");	
	$qst->setQuestion9("Have you identified a security problem?, Choose a value from 1 to 5 followed by the hash key.");
	$qst->setQuestion10("On behalf of fair makers we thank you for your time and wish you a safe and pleasant time.");
	$man->setManufacturer("Joker");
	$man->setLanguage("English");
	$qst->setManufacturer($man);
	$em = $this->getDoctrine()->getManager();
	$em->persist($man);
	$em->persist($qst);	
    	$em->flush();	
        return new Response(
            '<html><body>Entered Successfully</body></html>'
        );

}

    /**
     * @Route("/v/", name="makecall", methods="POST")
     */
    public function makeCall(Request $request)
    {
       // $to_num = "+33769287438";
	$to_num = "+918921487620";
	$man = "KFC";
	$lan = "English";
        $data = json_decode(file_get_contents('php://input'), true);
        if ($data) {
	
            foreach ($data as $item) {
		
                $to_num = $item['phone'];
		$man = $item['name'];
		$lan = $item['language'];
            
        
        $q_id = $this->getDoctrine()
            ->getRepository(Manufacturer::class)
            ->findOneBy(['language' => $lan, 'manufacturer' => $man]);
	if ($q_id){
	$qstn = $q_id->getQuestions();
	foreach ($qstn as $qt)
	{
	}
	$this->broadCast($to_num);}}}
        return new Response(
            '<html><body>Call is in progress to </body></html>'
        );


    }

    /**
     * @Route("/v/answer/", name="answer", methods="GET")
     */

    public function answer(Request $request)
    {

/*	$to = '8301092304';
	$from = '33186261610';
	$uuid = '74548b6f-1cbd-4958-a3ed-10c5f715ed2e';
	$conversation_uuid = 'Con-aaa7530b-e962-45a9-8455-676316121142';	*/
	$a = $request->query->get('to');
	$json = $request->getContent();
	$data = json_decode($json, true);
	$request->request->replace(is_array($data) ? $data : array());
	if ($data)
	{
	foreach ($data as $item)
	{
		$to = $item['to'];
		$from = $item['from'];
		$uuid = $item['uuid'];
		$conversation_uuid = $item['conversation_uuid'];
	}
	}	
	$ncoo = $GLOBALS['ncco'];

$nco = [
 [
        "action" => "talk",
        "voiceName" => "Jennifer",
	"text"=> "Hello and welcome to fair makers employee survey, please take a few minutes to answer the following questions"
    ],
    [
        "action" => "talk",
        "voiceName" => "Jennifer",
	"text"=> "Does your organization respect the prohibition of children working on the site?, Press 0 followed by hash for yes, 1 followed by hash for no.",
	"bargeIn"=> "true"
    ],
    [
        "action" => "input",
	"submitOnHash" => "True",
	"eventUrl" => ["http://54.175.197.6:8001/v/event/"], 
	"timeout" => 10
],
    [
        "action" => "talk",
        "voiceName" => "Jennifer",
	"text"=> "Have you been abused in the last 30 days?, Press 0 followed by hash for yes, 1 followed by hash for no.",
	"bargeIn"=> "true"
    ],
    [
        "action" => "input",
	"submitOnHash" => "True",
	"eventUrl" => ["http://54.175.197.6:8001/v/event/"], 
	"timeout" => 10
],
    [
        "action" => "talk",
        "voiceName" => "Jennifer",
	"text"=> "For the next questions choose a value between 1 and 5, with 1 being worst condition and 5 being the best condition."
    ]
  ,[
        "action" => "talk",
        "voiceName" => "Jennifer",
	"text"=> "Do you exceed the legal work hours?, Choose a value from 1 to 5 followed by the hash key.",
	"bargeIn"=> "true"
    ],
    [
        "action" => "input",
	"submitOnHash" => "True",
	"eventUrl" =>  ["http://54.175.197.6:8001/v/event/"], 
	"timeout" => 10
]
  ,[
        "action" => "talk",
        "voiceName" => "Jennifer",
	"text"=> "Do you have the right to join a union or workers representative association?, Choose a value from 1 to 5 followed by the hash key.",
	"bargeIn"=> "true"
    ],
    [
        "action" => "input",
	"submitOnHash" => "True",
	"eventUrl" => ["http://54.175.197.6:8001/v/event/"], 
	"timeout" => 10
]
  ,[
        "action" => "talk",
        "voiceName" => "Jennifer",
	"text"=> "Does your salary correspond at least to the minimum wage of your home country? , Choose a value from 1 to 5 followed by the hash key.",
	"bargeIn"=> "true"
    ],
    [
        "action" => "input",
	"submitOnHash" => "True",
	"eventUrl" => ["http://54.175.197.6:8001/v/event/"], 
	"timeout" => 10
]
  ,[
        "action" => "talk",
        "voiceName" => "Jennifer",
	"text"=> "Have you identified a lack of hygiene on your site?, Choose a value from 1 to 5 followed by the hash key.",
	"bargeIn"=> "true"
    ],
    [
        "action" => "input",
	"submitOnHash" => "True",
	"eventUrl" => ["http://54.175.197.6:8001/v/event/"], 
	"timeout" => 10
]
  ,[
        "action" => "talk",
        "voiceName" => "Jennifer",
	"text"=> "Have you identified a security problem?, Choose a value from 1 to 5 followed by the hash key.",
	"bargeIn"=> "true"
    ],
    [
        "action" => "input",
	"submitOnHash" => "True",
	"eventUrl" => ["http://54.175.197.6:8001/v/event/"], 
	"timeout" => 10
]
  ,[
        "action" => "talk",
        "voiceName" => "Jennifer",
        "text"=> "On behalf of fair makers we thank you for your time and wish you a safe and pleasant time."
    ]
];
//print_r($ncco);
/*       $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
        if ($data) {
		echo "inside";
            foreach ($data as $item) {
		echo $item['to'];
                $to = $item['to'];
                $uuid = $item['uuid'];
		$from = $item['from'];
            }
*/
//	print_r($nco);
	$ncco = json_encode($nco , true);
//	$ncco = json_encode($nco, true);
	return new Response($ncco, 200, array('Content-Type' => 'application/json'));
   // }
}
    /**
     * @Route("/v/event/", name="eventi", methods="POST")
     */

    public function eventCall(Request $request)
    {
	$count = 0;
	$session = $this->get('session');
	
	//$GLOBALS['ncco'] = $this->processNcco($qt);
        // Nexmo send a JSON payload to your event endpoint, so read and decode it
        $req = json_decode(file_get_contents('php://input'), true);
// Work with the call status
        if (isset($req['status'])) {
            switch ($req['status']) {
                case 'ringing':
                    $this->record_steps("UUID:".$req['uuid']."  - ringing.");
                    break;
                case 'answered':
                    $this->record_steps("UUID:i".$req['uuid']. " -was answered.");
		    $session->set('count', $count);
		    $session->set("child", "-1");
		    $session->set("abuse", "-1");
		    $session->set("hours", "-1");
		    $session->set("freedom", "-1");
		    $session->set("salary", "-1");
		    $session->set("security", "-1");
		    $session->set("sanitation", "-1");
		    //$feed = new Feedback();
                    break;
                case 'completed':
		    $count = 0;
		    $session->set('count', $count);
		    $sur = new Survey();
		    $sur->setUuid($req['uuid']);
		    $sur->setConversationUuid($req['conversation_uuid']);
		    $sur->setManufacturer("KFC");
	            $feed = new Feedback();
		    $feed->setChild((int)$session->get('child')); 	
		    $feed->setAbuse((int)$session->get('abuse')); 	
		    $feed->setHours((int)$session->get('hours')); 	
		    $feed->setFreedom((int)$session->get('freedom')); 	
		    $feed->setSalary((int)$session->get('salary')); 	
		    $feed->setSecurity((int)$session->get('security')); 	
		    $feed->setSanitation((int)$session->get('sanitation'));
		    //$sur->addFeedId($feed); 
		    //$em = $this->getDoctrine()->getManager();
		    //$entityManager->persist($sur);			
		    
		    $feed->setSurvey($sur);
		    $em = $this->getDoctrine()->getManager();
		    $em->persist($sur);
		    $em->persist($feed);
		    $em->flush();
                    $this->record_steps("UUID:".$req['uuid']."  - complete.");
                    break;
                default:
                    break;
            }
        }
//	$this->record_steps("UUID:  - ringing.");

	
	if(isset($req['dtmf']))
	{
//		$feed->setChild($req['dtmf']);
		$count = $session->get('count');
		switch($count)
		{
			case 0:
			$this->record_steps("Child->".$req['dtmf']);
			$session->set('child', $req['dtmf']);
			break;
			case 1:
			$this->record_steps("Abuse->".$req['dtmf']);
			$session->set('abuse', $req['dtmf']);
			break;
			case 2:
			$this->record_steps("Hours->".$req['dtmf']);
			$session->set('hours', $req['dtmf']);
			break;
			case 3:
			$this->record_steps("Freedom->".$req['dtmf']);
			$session->set('freedom', $req['dtmf']);
			break;
			case 4:
			$this->record_steps("Salary->".$req['dtmf']);
			$session->set('salary', $req['dtmf']);
			break;
			case 5:
			$this->record_steps("Security->".$req['dtmf']);
			$session->set('security', $req['dtmf']);
			break;
			case 6:
			$this->record_steps("Sanitation->".$req['dtmf']);
			$session->set('sanitation', $req['dtmf']);
			break;

		}
		$this->record_steps("uuid: ".$req['conversation_uuid']."anwser :".$req['dtmf']);
		$count++;
		$session->set('count', $count);
	}

return new Response();

    }

    private function broadCast($to)
    {
	$session = new Session(new NativeSessionStorage(), new AttributeBag());

        $credentials = $this->getDoctrine()->getRepository(NexmoCredential::class)
            ->findAll();
//	print_r($GLOBALS['ncco']);
        foreach ($credentials as $cred)
        {
        }

        $basic = new \Nexmo\Client\Credentials\Basic($cred->getApiKey(), $cred->getApiSecret());
        $keypair = new \Nexmo\Client\Credentials\Keypair(
            file_get_contents(__DIR__ . '/private.key'),
            $cred->getApplicationId()
        );

        $client = new \Nexmo\Client(new \Nexmo\Client\Credentials\Container($basic, $keypair));


        $client->calls()->create([
            'to' => [[
                'type' => 'phone',
                'number' => $to
            ]],
            'from' => [
                'type' => 'phone',
                'number' => '33186261610'
            ],
            'answer_url' => ['http://54.175.197.6:8001/v/answer/'],
            'event_url' => ['http://54.175.197.6:8001/v/event/'],
        ]);
        usleep(500000);
	$session->clear();

    }

    private function record_steps($message)
    {
        file_put_contents(__DIR__.'/call_log.txt', $message . PHP_EOL, FILE_APPEND | LOCK_EX);
    }

   private function processNcco(Question $question)
   {
   	$action1 = "talk";
	$action2 = "input";
	$bargeIn = "true";
	$submit = "true";
	$voice = "Jennifer";
	$timeout = 10;
	$url = "http://54.175.197.6:8001/v/event/";
	$question1 = $question->getQuestion1();
	$question2 = $question->getQuestion2();
	$question3 = $question->getQuestion3();
	$question4 = $question->getQuestion4();
	$question5 = $question->getQuestion5();
	$question6 = $question->getQuestion6();
	$question7 = $question->getQuestion7();
	$question8 = $question->getQuestion8();
	$question9 = $question->getQuestion9();
	$question10 = $question->getQuestion10();
	$ncco = [
 [
        "action" => $action1,
        "voiceName" => $voice,
	"text"=> $question1
    ],
    [
        "action" => $action1,
        "voiceName" => $voice,
	"text"=> $question2,
	"bargeIn"=> $bargeIn
    ],
    [
        "action" => $action2,
	"submitOnHash" => $submit,
	"eventUrl" => [$url], 
	"timeout" => $timeout
],
    [
        "action" => $action1,
        "voiceName" => $voice,
	"text"=> $question3,
	"bargeIn"=> $bargeIn
    ],
    [
        "action" => $action2,
	"submitOnHash" => $submit,
	"eventUrl" => [$url], 
	"timeout" => $timeout
],
    [
        "action" => $action1,
        "voiceName" => $voice,
	"text"=> $question4
    ]
  ,[
        "action" => $action1,
        "voiceName" => $voice,
	"text"=> $question5,
	"bargeIn"=> $bargeIn
    ],
    [
        "action" => $action2,
	"submitOnHash" => $submit,
	"eventUrl" => [$url], 
	"timeout" => $timeout
]
  ,[
        "action" => $action1,
        "voiceName" => $voice,
	"text"=> $question6,
	"bargeIn"=> $bargeIn
    ],
    [
        "action" => $action2,
	"submitOnHash" => $submit,
	"eventUrl" => [$url], 
	"timeout" => $timeout
]
  ,[
        "action" => $action1,
        "voiceName" => $voice,
	"text"=> $question7,
	"bargeIn"=> $bargeIn
    ],
    [
        "action" => $action2,
	"submitOnHash" => $submit,
	"eventUrl" => [$url], 
	"timeout" => $timeout
]
  ,[
        "action" => $action1,
        "voiceName" => $voice,
	"text"=> $question8,
	"bargeIn"=> $bargeIn
    ],
    [
        "action" => $action2,
	"submitOnHash" => $submit,
	"eventUrl" => [$url], 
	"timeout" => $timeout
]
  ,[
        "action" => $action1,
        "voiceName" => $voice,
	"text"=> $question9,
	"bargeIn"=> $bargeIn
    ],
    [
        "action" => $action2,
	"submitOnHash" => $submit,
	"eventUrl" => [$url], 
	"timeout" => $timeout
]
  ,[
        "action" => $action1,
        "voiceName" => $voice,
        "text"=> $question10
    ]
];
	json_encode($ncco, true);
	//print_r($ncco);
    return $ncco;
   }
}
