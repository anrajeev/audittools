<?php

namespace App\Repository;

use App\Entity\NexmoCredential;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method NexmoCredential|null find($id, $lockMode = null, $lockVersion = null)
 * @method NexmoCredential|null findOneBy(array $criteria, array $orderBy = null)
 * @method NexmoCredential[]    findAll()
 * @method NexmoCredential[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NexmoCredentialRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, NexmoCredential::class);
    }

    // /**
    //  * @return NexmoCredential[] Returns an array of NexmoCredential objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NexmoCredential
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
