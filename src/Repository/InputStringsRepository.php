<?php

namespace App\Repository;

use App\Entity\InputStrings;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InputStrings|null find($id, $lockMode = null, $lockVersion = null)
 * @method InputStrings|null findOneBy(array $criteria, array $orderBy = null)
 * @method InputStrings[]    findAll()
 * @method InputStrings[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InputStringsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InputStrings::class);
    }

    // /**
    //  * @return InputStrings[] Returns an array of InputStrings objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InputStrings
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
