<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FeedbackRepository")
 */
class Feedback
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $child;

    /**
     * @ORM\Column(type="integer")
     */
    private $abuse;

    /**
     * @ORM\Column(type="integer")
     */
    private $hours;

    /**
     * @ORM\Column(type="integer")
     */
    private $freedom;

    /**
     * @ORM\Column(type="integer")
     */
    private $salary;

    /**
     * @ORM\Column(type="integer")
     */
    private $sanitation;

    /**
     * @ORM\Column(type="integer")
     */
    private $security;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Survey", inversedBy="feedback")
     * @ORM\JoinColumn(nullable=false, name="survey_id", referencedColumnName="id")
     */
    private $survey;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChild(): ?int
    {
        return $this->child;
    }

    public function setChild(int $child): self
    {
        $this->child = $child;

        return $this;
    }

    public function getAbuse(): ?int
    {
        return $this->abuse;
    }

    public function setAbuse(int $abuse): self
    {
        $this->abuse = $abuse;

        return $this;
    }

    public function getHours(): ?int
    {
        return $this->hours;
    }

    public function setHours(int $hours): self
    {
        $this->hours = $hours;

        return $this;
    }

    public function getFreedom(): ?int
    {
        return $this->freedom;
    }

    public function setFreedom(int $freedom): self
    {
        $this->freedom = $freedom;

        return $this;
    }

    public function getSalary(): ?int
    {
        return $this->salary;
    }

    public function setSalary(int $salary): self
    {
        $this->salary = $salary;

        return $this;
    }

    public function getSanitation(): ?int
    {
        return $this->sanitation;
    }

    public function setSanitation(?int $sanitation): self
    {
        $this->sanitation = $sanitation;

        return $this;
    }

    public function getSecurity(): ?int
    {
        return $this->security;
    }

    public function setSecurity(?int $security): self
    {
        $this->security = $security;

        return $this;
    }

    public function getSurvey(): ?Survey
    {
        return $this->survey;
    }

    public function setSurvey(?Survey $survey): self
    {
        $this->survey = $survey;

        return $this;
    }


}
