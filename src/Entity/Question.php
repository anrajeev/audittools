<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuestionRepository")
 */
class Question
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $question1;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $question2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $question3;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $question4;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $question5;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $question6;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $question7;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $question8;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $question9;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Manufacturer", inversedBy="questions")
     * @ORM\JoinColumn(nullable=false, name="man_id", referencedColumnName="id")
     */
    private $manufacturer;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $question10;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuestion1(): ?string
    {
        return $this->question1;
    }

    public function setQuestion1(string $question1): self
    {
        $this->question1 = $question1;

        return $this;
    }

    public function getQuestion2(): ?string
    {
        return $this->question2;
    }

    public function setQuestion2(string $question2): self
    {
        $this->question2 = $question2;

        return $this;
    }

    public function getQuestion3(): ?string
    {
        return $this->question3;
    }

    public function setQuestion3(string $question3): self
    {
        $this->question3 = $question3;

        return $this;
    }

    public function getQuestion4(): ?string
    {
        return $this->question4;
    }

    public function setQuestion4(string $question4): self
    {
        $this->question4 = $question4;

        return $this;
    }

    public function getQuestion5(): ?string
    {
        return $this->question5;
    }

    public function setQuestion5(string $question5): self
    {
        $this->question5 = $question5;

        return $this;
    }

    public function getQuestion6(): ?string
    {
        return $this->question6;
    }

    public function setQuestion6(string $question6): self
    {
        $this->question6 = $question6;

        return $this;
    }

    public function getQuestion7(): ?string
    {
        return $this->question7;
    }

    public function setQuestion7(string $question7): self
    {
        $this->question7 = $question7;

        return $this;
    }

    public function getQuestion8(): ?string
    {
        return $this->question8;
    }

    public function setQuestion8(string $question8): self
    {
        $this->question8 = $question8;

        return $this;
    }

    public function getQuestion9(): ?string
    {
        return $this->question9;
    }

    public function setQuestion9(string $question9): self
    {
        $this->question9 = $question9;

        return $this;
    }

    public function getManufacturer(): ?Manufacturer
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?Manufacturer $manufacturer): self
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    public function getQuestion10(): ?string
    {
        return $this->question10;
    }

    public function setQuestion10(string $question10): self
    {
        $this->question10 = $question10;

        return $this;
    }
}
