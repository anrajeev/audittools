<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NexmoCredentialRepository")
 */
class NexmoCredential
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $api_key;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $api_secret;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $application_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getApiKey(): ?string
    {
        return $this->api_key;
    }

    public function setApiKey(string $api_key): self
    {
        $this->api_key = $api_key;

        return $this;
    }

    public function getApiSecret(): ?string
    {
        return $this->api_secret;
    }

    public function setApiSecret(string $api_secret): self
    {
        $this->api_secret = $api_secret;

        return $this;
    }

    public function getApplicationId(): ?string
    {
        return $this->application_id;
    }

    public function setApplicationId(string $application_id): self
    {
        $this->application_id = $application_id;

        return $this;
    }
}
